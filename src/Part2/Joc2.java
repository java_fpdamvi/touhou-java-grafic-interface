package Part2;
import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Joc2 {
	public static Field f = new Field();
	static Window w = new Window(f);
	public static void main(String[] args) throws InterruptedException{
		
		RocaControlable roca1 = new RocaControlable("roca",50,50,150,150,0,"resources/rock1.png",f);
		RocaControlable roca2 = new RocaControlable("roca",150,150,250,250,0,"resources/rock1.png",f,50);
		RocaControlable roca3 = new RocaControlable(250,250,350,350,f,50);
		RocaControlable roca4 = new RocaControlable(350,350,200,f);
		RocaControlable roca5 = new RocaControlable();
		
		ArrayList<Sprite> list = new ArrayList<Sprite>();
		list.add(roca1);
		list.add(roca2);
		list.add(roca3);
		list.add(roca4);
		list.add(roca5);
		
		boolean exit = false;

		while(!exit) {
			f.draw();
			Thread.sleep(30);
			input(roca2);
		}
	}
	public static void input(RocaControlable roca) {
		if(w.getPressedKeys().contains('d')) {
			roca.moviment(Input.DRETA);
		}
		if(w.getPressedKeys().contains('a')) {
			roca.moviment(Input.ESQUERRA);
		}
		if(w.getPressedKeys().contains('w')) {
			roca.moviment(Input.AMUNT);
		}
		if(w.getPressedKeys().contains('s')) {
			roca.moviment(Input.AVALL);
		}
	}
}
