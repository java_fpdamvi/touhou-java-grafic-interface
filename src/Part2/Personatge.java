package Part2;

import java.awt.Color;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Part1.roca;

public class Personatge extends PhysicBody{
	public boolean aterra;
	public int salts;
	public int saltsmax;
	
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String[] path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0,0.2);
		this.aterra = false;
		this.saltsmax = 2;
		this.salts = 2;
	}
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0,0.2);
		this.aterra = false;
		this.saltsmax = 2;
		this.salts = 2;
	}
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		this.setConstantForce(0,0.2);
		this.aterra = false;
		this.saltsmax = 2;
		this.salts = 2;
	}
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, Color color, Field f) {
		super(name, x1, y1, x2, y2, angle, color, f);
		this.setConstantForce(0,0.2);
		this.aterra = false;
		this.saltsmax = 2;
		this.salts = 2;
		
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof roca) {
			this.aterra = true;
			this.salts = 0;
		}
	}
	@Override
	public void onCollisionExit(Sprite sprite) {}
	
	public void moviment(Input2 in) {
		if (in==Input2.DRETA) {	
			this.setVelocity(6,velocity[1]);
		}else if(in==Input2.ESQUERRA) {
			this.setVelocity(-6,velocity[1]);
		}else if((in==Input2.SALT && this.aterra) || this.salts < this.saltsmax) {
			this.setForce(0,-2);
			this.aterra = false;
			this.salts++;
		}else if(in==Input2.QUIET) {
			this.setVelocity(0,velocity[1]);
		}
	}
	
	
	
	

}
