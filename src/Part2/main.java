package Part2;
import java.util.ArrayList;
import Core.Field;
import Core.Sprite;
import Core.Window;
import Part1.roca;

public class main {
	public static Field f = new Field();
	static Window w = new Window(f);
	public static void main(String[] args) throws InterruptedException {
		Personatge link = new Personatge("Link",300,300,450,450,0,"resources/Link1.gif",f);
		roca piso_up = new roca("piso_up",0,0,1900,150,0,"resources/rock1.png",f,0);
		roca piso_down = new roca("piso_down",0,900,1900,1000,0,"resources/rock1.png",f,0);
		boolean exit = false;
		while(!exit) {
			f.draw();
			Thread.sleep(30);
			input(link);
		}
	}
	private static void input(Personatge link) {
		if(w.getPressedKeys().contains('d')) {
			link.moviment(Input2.DRETA);
		}
		if(w.getPressedKeys().contains('a')) {
			link.moviment(Input2.ESQUERRA);
		}
		if(w.getKeysDown().contains('w')) {
			link.moviment(Input2.SALT);
		}
		if(w.getKeysUp().contains('d') || w.getKeysUp().contains('a')) {
			link.moviment(Input2.QUIET);
		}
		
		
	}

}
