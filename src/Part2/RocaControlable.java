package Part2;
import Core.Field;
import Part1.roca;

public class RocaControlable extends roca{

	//CONSTRUCTORS:
	public RocaControlable() {
		super();
	}

	public RocaControlable(int x1, int y1, int mida, Field f) {
		super(x1, y1, mida, f);
	}

	public RocaControlable(int x1, int y1, int x2, int y2, Field f, int accions) {
		super(x1, y1, x2, y2, f, accions);
	}

	public RocaControlable(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f,
			int accions) {
		super(name, x1, y1, x2, y2, angle, path, f, accions);
	}

	public RocaControlable(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
	}
	
	public void moviment(Input in){
		this.setAccionsDisponibles(this.getAccionsDisponibles()-1);
		if (in==Input.DRETA) {		
			this.x1++;
			this.x2++;
		}else if(in==Input.ESQUERRA) {
			this.x1--;
			this.x2--;
		}else if(in==Input.AMUNT) {
			this.y1--;
			this.y2--;
		}else if(in==Input.AVALL) {
			this.y1++;
			this.y2++;
		}
		if (this.getAccionsDisponibles() <= 0) {
			this.delete();
		}
	}

}
