package Part4;

import java.util.TimerTask;

import Core.Field;
import Core.Sprite;

public class Enemic_basic_movhor extends Enemic_basic{
	TimerTask task1 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   DispararEnemic();
	       }	
	};
	
	public Enemic_basic_movhor(String name, int x1, int y1, int x2, int y2, double angle, String path,Field f, int vida) {
		super(name, x1, y1, x2, y2, angle, path, f, vida);
		this.setVelocity(3, 0);
		timer.schedule(task1,1,1800);
		
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof Layout) {
			this.setVelocity(0, 0);
			this.setVelocity(-3, 0);
		}
	}

}
