package Part4;

import java.awt.Color;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Boss extends PhysicBody implements Disparable{
	int vida;
	static int total_vidas;
	static int count1;
	int dbala;
	ProjectilEnemic pe;
	Timer timer = new Timer();
	TimerTask atac1 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   if(total_vidas == 4) {
	    		   DispararEnemic();
	    	   }
	       }	
	};
	TimerTask atac2 = new TimerTask() {
	       @Override
	       public void run()
	       {	   
	    	   if(total_vidas == 3) {
	    		   if(count1 == 0) count1=1; else count1 = 0; 
	    	   }
		    	  
	       }	
	};
	TimerTask atac2_2 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   if(total_vidas == 3) {
	    		   DispararEnemic();
	    	   }
	       }
	};
	TimerTask atac3 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   if(total_vidas == 2) {
	    		   if(count1 == 0) count1=1; else count1 = 0;
	    		   DispararEnemic();
	    	   }
	       }	
	};
	TimerTask atac4 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   if(total_vidas == 1) {
	    		   DispararEnemic();
	    	   }
	       }	
	};
	public Boss(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.total_vidas = 4;
		this.vida = 1000;
		count1 = 1;
		this.dbala = -12;
		timer.schedule(atac1,2000,500);
		this.collisionBox();
		UI.getInstance().barra_de_vida.x2 = this.vida;
		UI.getInstance().barra_de_vida.color = Color.cyan;
		this.pe = new ProjectilEnemic("pe", 0, 0, 0, 0, 0, "resources/balaenemy.png", f);	
	}

	@Override
	public void danyar() {
		this.vida-=4;
		if(this.total_vidas == 1) {
			this.vida+=1;
		}
		if(this.vida <= 0) {
			this.total_vidas--;
			this.changeImage("resources/reimu_idle.gif");
			count1 = 0;
			if(this.total_vidas > 0) {
				this.vida = 1000;
				f.w.playSFX("resources/dmg_enemy.wav");
				switch(this.total_vidas) {
				case 3:
					UI.getInstance().barra_de_vida.color = Color.green;
					timer.schedule(atac2,2000, 3000);
					timer.schedule(atac2_2,2000, 100);
					break;
				case 2:
					UI.getInstance().barra_de_vida.color = Color.yellow;
					timer.schedule(atac3,2000, 500);
					break;
				case 1:
					UI.getInstance().barra_de_vida.color = Color.red;
					timer.schedule(atac4,4000, 1500);
					break;
				}
			}else {
				f.w.playSFX("resources/dmg_boss.wav");
				this.delete();
				UI.getInstance().punts.sumaPunts(30000);
				Spawner.oleada = -1;
				main.hayboss= false;
				main.bg1.changeImage("resources/Fondo.png");
				main.w.playMusic("resources/stage_theme.wav");
				timer.cancel();
			}
		}
		UI.getInstance().barra_de_vida.x2 = this.vida;
		
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {	
	}

	@Override
	public void onCollisionExit(Sprite sprite) {	
	}
	
	public void DispararEnemic() {
		if (f.containsSprite(this)) {
			if(total_vidas == 4) {
				this.changeImage("resources/reimu_atak.gif");
				if(count1>=1 && count1<=3) {
					ProjectilEnemic pe1 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe2 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe3 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe4 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe5 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					pe1.setVelocity(-2,10);
					pe2.setVelocity(-1,10);
					pe3.setVelocity(0,10);
					pe4.setVelocity(1,10);
					pe5.setVelocity(2,10);
				}else if(count1>=5 && count1<=7) {
					ProjectilEnemic pe1 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe2 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe3 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe4 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe5 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					pe1.setVelocity(-4,10);
					pe2.setVelocity(-3,10);
					pe3.setVelocity(-2,10);
					pe4.setVelocity(-1,10);
					pe5.setVelocity(0,10);
				}else if(count1>=9 && count1<=11) {
					ProjectilEnemic pe1 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe2 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe3 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe4 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe5 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					pe1.setVelocity(4,10);
					pe2.setVelocity(3,10);
					pe3.setVelocity(2,10);
					pe4.setVelocity(1,10);
					pe5.setVelocity(0,10);
				}
				else if(count1 > 13) {
					count1 = 0;
				}	
				count1++;
			}
			else if(total_vidas == 3) {
				this.changeImage("resources/reimu_atak.gif");
				if(count1 == 0) {
					ProjectilEnemic pe = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe2 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe3 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					pe.setVelocity(this.dbala,10);
					pe2.setVelocity(this.dbala-5,10);
					pe3.setVelocity(this.dbala+5,10);
					this.dbala--;
				}else {
					ProjectilEnemic pe = new ProjectilEnemic(this.pe,  this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe2 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					ProjectilEnemic pe3 = new ProjectilEnemic(this.pe, this.x1+100, this.y1+70, this.x2-70, this.y2-100);
					pe.setVelocity(this.dbala,10);	
					pe2.setVelocity(this.dbala-5,10);
					pe3.setVelocity(this.dbala+5,10);
					this.dbala++;
				}
			}
			else if(total_vidas == 2) {
				this.changeImage("resources/reimu_atak.gif");
				if(count1 == 0) {
					ProjectilEnemic pe1 = new ProjectilEnemic(this.pe, 0, 0, 50, 50);
					ProjectilEnemic pe2 = new ProjectilEnemic(this.pe, 100, 0, 150, 50);
					ProjectilEnemic pe3 = new ProjectilEnemic(this.pe, 200, 0, 250, 50);
					ProjectilEnemic pe4 = new ProjectilEnemic(this.pe, 300, 0, 350, 50);
					ProjectilEnemic pe5 = new ProjectilEnemic(this.pe, 400, 0, 450, 50);
					ProjectilEnemic pe6 = new ProjectilEnemic(this.pe, 500, 0, 550, 50);
					ProjectilEnemic pe7 = new ProjectilEnemic(this.pe, 600, 0, 650, 50);
					ProjectilEnemic pe8 = new ProjectilEnemic(this.pe, 700, 0, 750, 50);
					ProjectilEnemic pe9 = new ProjectilEnemic(this.pe, 800, 0, 850, 50);
					ProjectilEnemic pe10 = new ProjectilEnemic(this.pe, 900, 0, 950, 50);
					pe1.setVelocity(0,9);
					pe2.setVelocity(0,9);
					pe3.setVelocity(0,9);
					pe4.setVelocity(0,9);
					pe5.setVelocity(0,9);
					pe6.setVelocity(0,9);
					pe7.setVelocity(0,9);
					pe8.setVelocity(0,9);
					pe9.setVelocity(0,9);
					pe10.setVelocity(0,9);
				}else {
					ProjectilEnemic pe1 = new ProjectilEnemic(this.pe, 50, 0, 100, 50);
					ProjectilEnemic pe2 = new ProjectilEnemic(this.pe, 150, 0, 200, 50);
					ProjectilEnemic pe3 = new ProjectilEnemic(this.pe, 250, 0, 300, 50);
					ProjectilEnemic pe4 = new ProjectilEnemic(this.pe, 350, 0, 400, 50);
					ProjectilEnemic pe5 = new ProjectilEnemic(this.pe, 450, 0, 500, 50);
					ProjectilEnemic pe6 = new ProjectilEnemic(this.pe, 550, 0, 600, 50);
					ProjectilEnemic pe7 = new ProjectilEnemic(this.pe, 650, 0, 700, 50);
					ProjectilEnemic pe8 = new ProjectilEnemic(this.pe, 750, 0, 800, 50);
					ProjectilEnemic pe9 = new ProjectilEnemic(this.pe, 850, 0, 900, 50);
					ProjectilEnemic pe10 = new ProjectilEnemic(this.pe, 950, 0, 1000, 50);
					pe1.setVelocity(0,9);
					pe2.setVelocity(0,9);
					pe3.setVelocity(0,9);
					pe4.setVelocity(0,9);
					pe5.setVelocity(0,9);
					pe6.setVelocity(0,9);
					pe7.setVelocity(0,9);
					pe8.setVelocity(0,9);
					pe9.setVelocity(0,9);
					pe10.setVelocity(0,9);
				}
			}else if(total_vidas == 1) {
				this.changeImage("resources/reimu_atak.gif");
				ProjectilEnemic pe1 = new ProjectilEnemic(this.pe,this.x1-100,this.y1+100,this.x1-50,this.y1+150);
				ProjectilEnemic pe2 = new ProjectilEnemic(this.pe,this.x2+100,this.y1+100,this.x2+150,this.y1+150);
				ProjectilEnemic pe3 = new ProjectilEnemic(this.pe,this.x1+100,this.y1-100,this.x1+150,this.y1-50);
				ProjectilEnemic pe4 = new ProjectilEnemic(this.pe,this.x1+100,this.y2+75,this.x1+150,this.y2+125);
				ProjectilEnemic pe5 = new ProjectilEnemic(this.pe,this.x1-50,this.y1-50,this.x1,this.y1);
				ProjectilEnemic pe6 = new ProjectilEnemic(this.pe,this.x2+50,this.y1-50,this.x2+100,this.y1);
				ProjectilEnemic pe7 = new ProjectilEnemic(this.pe,this.x1-50,this.y2+50,this.x1,this.y2+100);
				ProjectilEnemic pe8 = new ProjectilEnemic(this.pe,this.x2+50,this.y2+50,this.x2+100,this.y2+100);
				ProjectilEnemic pe9 = new ProjectilEnemic(this.pe,this.x1+25,this.y1+25,this.x1+75,this.y1+75);
				ProjectilEnemic pe10 = new ProjectilEnemic(this.pe,this.x2-25,this.y1+25,this.x2+25,this.y1+75);
				ProjectilEnemic pe11 = new ProjectilEnemic(this.pe,this.x1+25,this.y2-25,this.x1+75,this.y2+25);
				ProjectilEnemic pe12 = new ProjectilEnemic(this.pe,this.x2-25,this.y2-25,this.x2+25,this.y2+25);
				pe1.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe2.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe3.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe4.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe5.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe6.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe7.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe8.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe9.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe10.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe11.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
				pe12.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
			}		
		}
		
	}
	public void collisionBox() {
		this.drawingBoxExtraLeft = 70;
		this.drawingBoxExtraRight = 70;
		this.drawingBoxExtraUp = 70;
		this.drawingBoxExtraDown = 70;
		this.collisionBox = true;
	}	
}
