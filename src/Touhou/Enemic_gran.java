package Part4;

import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.Sprite;

public class Enemic_gran extends Enemic{
	boolean direccio = true;
	int dbala = -12;
	Timer timer = new Timer();
	TimerTask task1 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   if(direccio)direccio = false;
	    	   else direccio = true;
	    		  
	       }	
	};
	TimerTask task2 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   DispararEnemic();		  
	       }	
	};
	public Enemic_gran(String name, int x1, int y1, int x2, int y2, double angle, String path,Field f, int vida) {
		super(name, x1, y1, x2, y2, angle, path, f, vida);
		this.pe = new ProjectilEnemic("pe", 0, 0, 0, 0, 0, "resources/balaenemy.png", f);
		this.setVelocity(0, 0.000000000000000000000000001);
		timer.schedule(task1,1,3000);
		timer.schedule(task2,1,100);
	}
	
	public void DispararEnemic(){
		if (f.containsSprite(this)) {
			if(this.direccio) {
				ProjectilEnemic pe = new ProjectilEnemic(this.pe, this.x1+30, this.y1+30, this.x2-30, this.y2-30);
				pe.setVelocity(this.dbala,10);	
				dbala--;
			}else {
				ProjectilEnemic pe = new ProjectilEnemic(this.pe, this.x1+30, this.y1+30, this.x2-30, this.y2-30);
				pe.setVelocity(this.dbala,10);	
				dbala++;
			}
					
		}
	}
	public void spriteDelete() {
		this.delete();
	}
}
