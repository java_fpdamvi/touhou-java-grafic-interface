package Part4;

import Core.Field;
import Core.Sprite;

public class Puntuacio extends Sprite{
	static int punts = 0;
	public Puntuacio(String name, int x1, int y1, int x2, int y2, double angle, Field f) {
		super(name, x1, y1, x2, y2, angle,"Puntuació: "+punts, f);
		this.solid = false;
		this.text = true;
		this.colorSprite = true;
		this.textColor = 0xFFFFFF;
	}
	
	void sumaPunts(int num) {
		punts += num;
		this.path = "Puntuacio: "+punts;
	}
}
