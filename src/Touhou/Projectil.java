package Part4;
import Core.PhysicBody;
import Core.Sprite;
import Core.Field;
public class Projectil extends PhysicBody{
	
	public Projectil(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		
	}
	public Projectil(Projectil p, float x1,float y1,float x2,float y2) {
		super(p.name, (int)x1,(int) y1,(int) x2,(int) y2, p.angle, p.path,p.f);
		this.trigger = true;
		this.setVelocity(0,-20);
		
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof Layout) {
			this.delete();
		}
		if(sprite instanceof Boss) {
			((Boss)sprite).danyar();
			this.delete();
		}
	}
	@Override
	public void onCollisionExit(Sprite sprite) {	
	}
	
	@Override
	public void update() {
	    if(this.y1<0){
	        this.delete();
	    }
	}

	
}
