package Part4;

import java.util.Timer;
import java.util.TimerTask;

import Core.Field;

public class Enemic_basic extends Enemic{
	ProjectilEnemic pe;
	Timer timer = new Timer();
	public Enemic_basic(String name, int x1, int y1, int x2, int y2, double angle, String path,Field f, int vida) {
		super(name, x1, y1, x2, y2, angle, path, f, vida);
		this.pe = new ProjectilEnemic("pe", 0, 0, 0, 0, 0, "resources/balaenemy.png", f);	
	}
	
	public void DispararEnemic() {
		if (f.containsSprite(this) && this.y2>0) {
			ProjectilEnemic pe1 = new ProjectilEnemic(this.pe, this.x1+30, this.y1+30, this.x2-30, this.y2-30);
			ProjectilEnemic pe2 = new ProjectilEnemic(this.pe, this.x1+30, this.y1+30, this.x2-30, this.y2-30);
			ProjectilEnemic pe3 = new ProjectilEnemic(this.pe, this.x1+30, this.y1+30, this.x2-30, this.y2-30);
			ProjectilEnemic pe4 = new ProjectilEnemic(this.pe, this.x1+30, this.y1+30, this.x2-30, this.y2-30);
			ProjectilEnemic pe5 = new ProjectilEnemic(this.pe, this.x1+30, this.y1+30, this.x2-30, this.y2-30);
			pe1.setVelocity(-2.5,5);
			pe2.setVelocity(-1.5,5);
			pe3.setVelocity(0,5);
			pe4.setVelocity(1.5,5);
			pe5.setVelocity(2.5,5);
		}
	}
	
}
