package Part4;

import java.util.TimerTask;

import Core.Field;

public class Enemic_basic_movdown extends Enemic_basic{
	TimerTask task1 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   DispararEnemic();
	       }	
	};
	public Enemic_basic_movdown(String name, int x1, int y1, int x2, int y2, double angle, String path,Field f, int vida) {
		super(name, x1, y1, x2, y2, angle, path, f, vida);
		this.setVelocity(0, 3);
		timer.schedule(task1,1,1800);
	}

}
