package Part4;

import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Random;
import Core.Field;
import Core.Observer;
import Core.PhysicBody;
import Core.Sprite;

public class Nau extends PhysicBody implements Observer{
	public Projectil projectil;
	boolean disparando=false;
	boolean candisparar = true;
	static int vida;
	static double x_nau;
	static double y_nau;
	static boolean no_atacar_boss;
	Timer timer = new Timer();
	TimerTask task1 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   camviDisparar();
	       }
	};
	public Nau(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, Projectil p) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.projectil = p;
		this.collisionBox();
		this.f.subscribe(this);
		this.f.w.subscribe(this);
		this.vida = 3;
		no_atacar_boss = true;
		timer.schedule(task1,1,80);
	}
	
	public void camviDisparar() {
		this.candisparar = true;
	} 

	@Override
	public void onCollisionEnter(Sprite sprite) {
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
	}
	
	
	@Override
	public void update() {
		if(disparando && candisparar && no_atacar_boss)disparar();
		x_nau = this.x1-50;
		y_nau = this.y1-50;
	}

	public void disparar() {
		Random r = new Random();
		Projectil projectil1 = new Projectil(this.projectil, this.x1-35, this.y1-50, this.x1, this.y1-20);
		Projectil projectil2 = new Projectil(this.projectil, this.x1+5, this.y1-50, this.x1+40, this.y1-20);
		int num1 = r.nextInt(1,6);
		int num2 = r.nextInt(1,6);
		randomSprite(projectil1,num1);
		randomSprite(projectil2,num2);
		this.candisparar = false;
		
	}
	private void randomSprite(Projectil projectil1, int num1) {
		switch(num1) {
		case 1:
			projectil1.changeImage("resources/estrella1.png");
			break;
		case 2:
			projectil1.changeImage("resources/estrella2.png");
			break;
		case 3:
			projectil1.changeImage("resources/estrella3.png");
			break;
		case 4:
			projectil1.changeImage("resources/estrella4.png");
			break;
		case 5:
			projectil1.changeImage("resources/estrella5.png");
			break;
		default:
			projectil1.changeImage("resources/estrella1.png");
			break;
		}
	}
	public void collisionBox() {
		this.drawingBoxExtraLeft = 25;
		this.drawingBoxExtraRight = 25;
		this.drawingBoxExtraUp = 35;
		this.drawingBoxExtraDown = 25;
		this.collisionBox = true;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_Z) {
			disparando=true;
		}
		if(e.getKeyCode()==KeyEvent.VK_LEFT) {
			if(this.x1>1) {
				this.setVelocity(-9,velocity[1]);
			}else {
				this.setVelocity(0,0);
			}
			this.changeImage("resources/marisa_left.png");
		}
		if(e.getKeyCode()==KeyEvent.VK_RIGHT) {
			this.setVelocity(9,velocity[1]);
			this.changeImage("resources/marisa_right.png");
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		if(e.getKeyCode()==KeyEvent.VK_LEFT && this.velocity[0]<0f || e.getKeyCode()==KeyEvent.VK_RIGHT && this.velocity[0]>0f) {
			this.setVelocity(0,0);
			this.changeImage("resources/marisa_idle.png");
		}
		if(e.getKeyCode()==KeyEvent.VK_Z) disparando=false;
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}		
	}


