package Part4;
import java.util.ArrayList;
import Core.Field;
import Core.Sprite;
import Core.Window;

public class main {
	
	public static Field f = new Field();
	static Window w = new Window(f);
	static boolean hayboss = false;
	static Sprite bg1 = new Sprite("background",0,0,1000,1000,0,"resources/Fondo.png",f);
	static Sprite bg2 = new Sprite("background",0,-1000,1000,0,0,"resources/Fondo.png",f);
	public static void main(String[] args) throws InterruptedException {
		w.musicVolume=85;
		w.sfxVolume=70;
		w.setSize(1200, 1000);
		bg1.solid=false;
		bg1.orderInLayer=-100;
		bg2.solid=false;
		bg2.orderInLayer=-100;
		Layout layout = new Layout("layout", 1000, 0, 1600, 1200, 0,"resources/layout.png", f);
		ProjectilEnemic pe = new ProjectilEnemic("pe", 0, 0, 0, 0, 0,"resources/balaenemy.png", f);
		Projectil projectil = new Projectil("projectil", 0, 0, 0, 0, 0,"resources/estrella1.png", f);
		Nau marisa = new Nau("marisa", 500, 900, 505, 915, 0, "resources/marisa_idle.png", f,projectil);
		Spawner spawner = new Spawner(f);
		boolean exit = false;
		main.w.playMusic("resources/stage_theme.wav");
		while(!exit) {
			UI ui = UI.getInstance();
			f.draw();
			Thread.sleep(30);
			if(!hayboss) {
				bg1.y1 += 5;
				bg1.y2 += 5;
				bg2.y1 += 5;
				bg2.y2 += 5;
				if(bg1.y1 > 990) {
					bg1.y1 = -1000;
					bg1.y2 = 0;
				}
				if(bg2.y1 > 990) {
					bg2.y1 = -1000;
					bg2.y2 = 0;
				}	
			}
			if(!f.containsSprite(marisa)) {
				exit = true;
			}
		}
	}

}
