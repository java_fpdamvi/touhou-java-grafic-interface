package Part4;

import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Enemic extends PhysicBody implements Disparable{
	public int vida;
	ProjectilEnemic pe;
	Timer timer = new Timer();
	TimerTask task2 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   spriteDelete();
	       }	
	};
	public Enemic(String name, int x1, int y1, int x2, int y2, double angle, String path,Field f,int vida) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.vida = vida;
		this.trigger = true;
		timer.schedule(task2,13000);
	}
	
	@Override
	public void onTriggerEnter(Sprite sprite) {
		if(sprite instanceof Projectil) {
			this.danyar();
			sprite.delete();
		}
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof Nau) {
			Nau.vida--;
			UI.changeVida(Nau.vida);
			this.delete();
			f.w.playSFX("resources/dead.wav");
			if(Nau.vida < 0) {
				sprite.delete();
			}
		}	
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
	}
	
	@Override
	public void danyar() {
		this.vida--;
		if (this.vida <= 0) {
			UI.getInstance().punts.sumaPunts(1000);
			if(this instanceof Enemic_gran) {
				UI.getInstance().punts.sumaPunts(4000);
			}
			f.w.playSFX("resources/dmg_enemy.wav");
			this.delete();	
		}
	}
	
	@Override
	public void update() {
	    if(this.y2<-500){
	        this.delete();
	    }
	    if(this.y2>1200){
	        this.delete();
	    }
	    if(this.x1 < -100) {
	    	this.delete();
	    }
	}
	
	public void spriteDelete() {
		this.delete();
	}
}
