package Part4;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class ProjectilEnemic extends PhysicBody{

	public ProjectilEnemic(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
	}
	public ProjectilEnemic(ProjectilEnemic p, float x1,float y1,float x2,float y2) {
		super(p.name, (int)x1,(int) y1,(int) x2,(int) y2, p.angle, p.path,p.f);
		this.trigger = true;
		this.drawingBoxExtraLeft = 10;
		this.drawingBoxExtraRight = 10;
		this.drawingBoxExtraUp = 10;
		this.drawingBoxExtraDown = 10;
		this.collisionBox = true;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof Nau) {
			Nau.vida--;
			UI.changeVida(Nau.vida);
			this.delete();
			f.w.playSFX("resources/dead.wav");
			if(Nau.vida < 0) {
				sprite.delete();
			}
		}
		if(sprite instanceof Layout) {
			this.delete();
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		
	}
	
	@Override
	public void update() {
	    if(this.y2>1100){
	        this.delete();
	    }
	}

}
