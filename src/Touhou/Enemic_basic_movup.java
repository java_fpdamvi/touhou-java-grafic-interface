package Part4;

import java.util.TimerTask;

import Core.Field;

public class Enemic_basic_movup extends Enemic_basic{
	int moviment;
	TimerTask task1 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   moviment();
	    	   DispararEnemic();
	       }	
	};
	public Enemic_basic_movup(String name, int x1, int y1, int x2, int y2, double angle, String path,Field f, int vida) {
		super(name, x1, y1, x2, y2, angle, path, f, vida);
		this.setVelocity(0, 4);
		timer.schedule(task1,1,1800);
		this.moviment = 0;
	}
	
		public void moviment() {
		this.moviment++;
		if(this.moviment == 3) {
			this.setVelocity(0, 0);
			this.setVelocity(0, -4);
		}
	}
}
