package Part4;

import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.Sprite;

public class Enemic_seguidor extends Enemic{
	Timer timer = new Timer();
	TimerTask task1 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   DispararEnemic();
	       }	
	};
	public Enemic_seguidor(String name, int x1, int y1, int x2, int y2, double angle, String path,Field f, int vida) {
		super(name, x1, y1, x2, y2, angle, path, f, vida);
		this.pe = new ProjectilEnemic("pe", 0, 0, 0, 0, 0, "resources/balaenemy.png", f);
		this.setVelocity(0, 0.000000000000000000000000001);
		timer.schedule(task1,1,400);
	}
	
	public void DispararEnemic() {
		if (f.containsSprite(this)) {
			ProjectilEnemic pe = new ProjectilEnemic(this.pe, this.x1+30, this.y1+30, this.x2-30, this.y2-30);
			//Esta linea de código ha sido creada gracias a David, que almenos sabe matemáticas básicas:
			pe.setVelocity((Nau.x_nau-this.x1)/60,(Nau.y_nau-this.y1)/60);
		}
	}
}
