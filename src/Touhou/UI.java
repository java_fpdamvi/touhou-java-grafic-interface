package Part4;

import java.awt.Color;

import Core.Sprite;

public class UI {
	 public static UI instance = null;
	    public Puntuacio punts;
	    static Sprite vida;
	    static Sprite barra_de_vida;
	    private UI() {
	    	this.punts = new Puntuacio("punts", 1060, 50, 1200, 100, 0, main.f);
	    	this.vida = new Sprite("vida", 1030, 100,1230, 200, 0, "resources/vidas3.png", main.f);
	    	this.barra_de_vida = new Sprite("barra_de_vida", 0, 20, 0, 50, 0, Color.blue, main.f);
	    }
	    public static UI getInstance() {
	        if(instance == null) {
	            instance = new UI();
	        }
	        return instance;
	    }
	    
	    static public void changeVida(int num){
	    	switch(num) {
	    	case 3:
	    		vida.changeImage("resources/vidas3.png");
	    		break;
	    	case 2:
	    		vida.changeImage("resources/vidas2.png");
	    		break;
	    	case 1:
	    		vida.changeImage("resources/vidas1.png");
	    		break;
	    	default:
	    		vida.changeImage("");
	    		break;
	    	}
    	}
}
