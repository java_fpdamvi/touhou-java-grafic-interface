package Part4;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;

public class Spawner {
	Field f;
	static int oleada;
	Timer timer = new Timer();
	TimerTask task1 = new TimerTask() {
	       @Override
	       public void run()
	       {
	    	   generarEnemic();
	       }
	};
	public Spawner(Field f){
		this.f = f;
		this.oleada = 0;
		timer.schedule(task1,3000,6000);
	}
	
	public void generarEnemic() {
		this.oleada++;
		switch(oleada) {
		case 1:
			Enemic_basic_movup enemy1_1 = new Enemic_basic_movup("fairy", 100, 0, 100+75, 70, 0, "resources/enemy.png", f, 20);
			Enemic_basic_movup enemy2_1 = new Enemic_basic_movup("fairy", 800, 0, 800+75, 70, 0, "resources/enemy.png", f, 20);
			break;
		case 2:
			Enemic_basic_movdown enemyprimero_2 = new Enemic_basic_movdown("fairy", 450, 0, 450+75, 70, 0, "resources/enemy.png", f, 20);
			Enemic_basic_movdown enemysegundo_2 = new Enemic_basic_movdown("fairy", 450, -100, 450+75, -30, 0, "resources/enemy.png", f, 20);
			Enemic_basic_movdown enemytercero_2  = new Enemic_basic_movdown("fairy", 450, -200, 450+75, -130, 0, "resources/enemy.png", f, 20);
			break;
		case 3:
			Enemic_basic_movup enemyizq1_3 = new Enemic_basic_movup("fairy", 100, 0, 100+75, 70, 0, "resources/enemy.png", f, 20);
			Enemic_basic_movup enemyder1_3 = new Enemic_basic_movup("fairy", 800, 0, 800+75, 70, 0, "resources/enemy.png", f, 20);
			Enemic_basic_movdown enemyizq2_3 = new Enemic_basic_movdown("fairy", 300, -100, 300+75, -30, 0, "resources/enemy.png", f, 20);
			Enemic_basic_movdown enemyder2_3 = new Enemic_basic_movdown("fairy", 600, -100, 600+75, -30, 0, "resources/enemy.png", f, 20);
			Enemic_basic_movdown enemyup_3 = new Enemic_basic_movdown("fairy", 450, -200, 450+75, -130, 0, "resources/enemy.png", f, 20);
			break;
		case 4:
			Enemic_basic_movhor enemyizq1_4 = new Enemic_basic_movhor("fairy", 0, 200, 0+75, 270, 0, "resources/enemy.png", f, 20);
			Enemic_basic_movhor enemyizq2_4 = new Enemic_basic_movhor("fairy", 0, 350, 0+75, 420, 0, "resources/enemy.png", f, 20);
			Enemic_basic_movup enemyup_4= new Enemic_basic_movup("fairy", 450, -200, 450+75, -130, 0, "resources/enemy.png", f, 20);
			break;
		case 5:
			Enemic_seguidor enemyseg_5 = new Enemic_seguidor("fairy", 100, 100, 100+75, 170, 0, "resources/enemy2.png", f, 20);
			break;
		case 6:
			Enemic_seguidor enemyseg_6 = new Enemic_seguidor("fairy", 800, 100, 800+75, 170, 0, "resources/enemy2.png", f, 20);
			break;
		case 7:
			Enemic_seguidor enemysegizq_7 = new Enemic_seguidor("fairy", 100, 100, 100+75, 170, 0, "resources/enemy2.png", f, 20);
			Enemic_seguidor enemysegder_7 = new Enemic_seguidor("fairy", 800, 100, 800+75, 170, 0, "resources/enemy2.png", f, 20);
			Enemic_basic_movdown enemyup_7= new Enemic_basic_movdown("fairy", 450, 0, 450+75, 70, 0, "resources/enemy.png", f, 20);
			break;
		case 8:
			Enemic_seguidor enemysegizq_8 = new Enemic_seguidor("fairy", 100, 200, 100+75, 270, 0, "resources/enemy2.png", f, 20);
			Enemic_seguidor enemysegder_8 = new Enemic_seguidor("fairy", 800, 200, 800+75, 270, 0, "resources/enemy2.png", f, 20);
			Enemic_seguidor enemysegcentro_8 = new Enemic_seguidor("fairy", 800, 200, 800+75, 270, 0, "resources/enemy2.png", f, 20);
			Enemic_basic_movhor enemyhor_8 = new Enemic_basic_movhor("fairy", 0, 350, 0+75, 420, 0, "resources/enemy.png", f, 20);
			break;
		case 10:
			Enemic_gran enemy_9 = new Enemic_gran("fairy", 450, 0, 550, 100, 0, "resources/enemy.png", f, 70);
			break;
		case 11:
			Enemic_gran enemy1_10 = new Enemic_gran("fairy", 100, 0, 200, 100, 0, "resources/enemy.png", f, 60);
			Enemic_gran enemy2_10 = new Enemic_gran("fairy", 750, 0, 850, 100, 0, "resources/enemy.png", f, 60);
			break;
		case 13:
			Nau.no_atacar_boss = false;
			main.hayboss = true;
			main.bg2.y1 = -1000;
			main.bg2.y2 = 0;
			main.bg1.y1 = 0;
			main.bg1.y2 = 1000;
			main.bg1.changeImage("resources/fondo_boss2.png");
			break;
		case 14:
			Boss boss = new Boss("Reimu", 400, 200, 600, 400, 0, "resources/reimu_idle.gif", f);
			Nau.no_atacar_boss = true;
			main.w.playMusic("resources/boss_theme.wav");
			break;
		}
		
		
	}
}
