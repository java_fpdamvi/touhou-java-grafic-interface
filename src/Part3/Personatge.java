package Part3;

import java.awt.Color;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import Part1.roca;

public class Personatge extends PhysicBody{
	public boolean aterra;
	public int salts;
	public int saltsmax;
	public boolean seeright; 
	public Projectil projectil;
	
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String[] path, Field f, Projectil p) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0,0.4);
		this.aterra = false;
		this.saltsmax = 2;
		this.salts = 2;
		this.projectil = p;
		this.seeright = true;
	}
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, Projectil p) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0,0.4);
		this.aterra = false;
		this.saltsmax = 2;
		this.salts = 2;
		this.projectil = p;
		this.seeright = true;
	}
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String path, Projectil p) {
		super(name, x1, y1, x2, y2, angle, path);
		this.setConstantForce(0,0.4);
		this.aterra = false;
		this.saltsmax = 2;
		this.salts = 2;
		this.projectil = p;
		this.seeright = true;
	}
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, Color color, Field f, Projectil p) {
		super(name, x1, y1, x2, y2, angle, color, f);
		this.setConstantForce(0,0.4);
		this.aterra = false;
		this.saltsmax = 2;
		this.salts = 2;
		this.projectil = p;
		this.seeright = true;
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof roca) {
			this.aterra = true;
			this.salts = 0;
		}
		if(sprite instanceof Enemic) {
			if (this.y1 > sprite.y1) {
				this.delete();
			}else {
				sprite.delete();
				this.setVelocity(velocity[0],0);
				this.setForce(force[0],0);
				this.addForce(0,-1.5);
			}
		}
	}
	@Override
	public void onCollisionExit(Sprite sprite) {}
	
	public void moviment(Input in) {
		if (in==Input.DRETA) {	
			this.setVelocity(6,velocity[1]);
			this.flippedX = false;
			this.seeright = true;
		}else if(in==Input.ESQUERRA) {
			this.setVelocity(-6,velocity[1]);
			this.flippedX = true;
			this.seeright = false;
		}else if ((in==Input.SALT && this.aterra) || (in==Input.SALT && this.salts < this.saltsmax)) {
			this.setVelocity(velocity[0],0);
			this.setForce(force[0],0);
			this.addForce(0,-3.1);
			this.aterra = false;
			this.salts++;
		}else if(in==Input.QUIET) {
			this.setVelocity(0,velocity[1]);
		}
	}
	public void disparar() {
		if (this.seeright) {
			Projectil projectil = new Projectil(this.projectil, this.x1+50, this.y1+50, this.x1+100, this.y1+100,15);
		}else {
			Projectil projectil = new Projectil(this.projectil, this.x1+50, this.y1+50, this.x1+100, this.y1+100,-15);
			projectil.flippedX = true;
		}
			
	}
}
