package Part3;
import Core.PhysicBody;
import Core.Sprite;
import Part1.roca;
import Core.Field;
public class ProjectilEnemic extends PhysicBody{
	public int x_inicial;
	
	public ProjectilEnemic(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.trigger = true;
		this.setVelocity(-15,0);
		this.x_inicial = x1;
		
	}
	public ProjectilEnemic(ProjectilEnemic p, float x1,float y1,float x2,float y2) {
		super(p.name, (int)x1,(int) y1,(int) x2,(int) y2, p.angle, p.path,p.f);
		this.trigger = true;
		this.setVelocity(-15,0);
		this.x_inicial = (int)x1;
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof roca) {
			this.delete();
		}
		if(sprite instanceof Personatge) {
			sprite.delete();
			this.delete();
		}
	}
	@Override
	public void onCollisionExit(Sprite sprite) {	
	}
	
	@Override
	public void update() {
		if(this.x1 < this.x_inicial-750) {
			this.delete();
		}
	}
}
