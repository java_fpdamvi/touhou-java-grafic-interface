package Part3;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Obstacle extends PhysicBody implements Disparable{
	public int vida;
	
	public Obstacle(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int vida) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.vida = vida;
		this.setConstantForce(0, 0.2);
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
	}

	@Override
	public void onCollisionExit(Sprite sprite) {	
	}

	@Override
	public void danyar() {
		this.delete();
	}

	
}
