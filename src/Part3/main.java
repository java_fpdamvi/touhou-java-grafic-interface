package Part3;
import java.util.ArrayList;
import Core.Field;
import Core.Sprite;
import Core.Window;
import Part1.roca;

public class main {
	public static Field f = new Field();
	static Window w = new Window(f);
	public static void main(String[] args) throws InterruptedException {
		ProjectilEnemic pe = new ProjectilEnemic("projectilE", 0, 0, 0, 0, 0,"resources/rockprojectil.gif", f);
		Enemic enemy = new Enemic("enemic", 1400,550, 1550, 700,0, "resources/octorok.gif", f, 5, pe);
		Obstacle obs = new Obstacle("enemic", 1400, 700, 1550, 850,0, "resources/joan.jpg", f, 5);
		Projectil projectil = new Projectil("projectil", 0, 0, 0, 0, 0,"resources/arrow.png", f);
		Personatge link = new Personatge("Link",200,700,350,850,0,"resources/Linkcdright.png",f,projectil);
		roca piso_down = new roca("piso_down",0,900,3000,1000,0,"resources/grass.jpg",f,0);
		roca plataforma = new roca("plataforma",2000,700,2300,730,0,"resources/grass.jpg",f,0);
		roca plataforma2 = new roca("plataforma",500,700,800,730,0,"resources/grass.jpg",f,0);
		boolean exit = false;
		f.lockScroll(link,w);
		f.lockScrollY = false;
		int contador = 0;
		while(!exit) {
			f.draw();
			Thread.sleep(30);
			input(link);
			if (contador%50 == 0) {
				enemy.disparar();
			}
			contador++;
		}
	}
	private static void input(Personatge link) {
		if(w.getPressedKeys().contains('d')) {
			link.moviment(Input.DRETA);
		}
		if(w.getPressedKeys().contains('a')) {
			link.moviment(Input.ESQUERRA);
		}
		if(w.getKeysDown().contains('w')) {
			link.moviment(Input.SALT);
		}
		if(w.getKeysUp().contains('d') || w.getKeysUp().contains('a')) {
			link.moviment(Input.QUIET);
		}
		if(w.getKeysDown().contains(' ')) {
			link.disparar();
		}
		
	}

}

