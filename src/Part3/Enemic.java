package Part3;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Enemic extends PhysicBody implements Disparable{
	public int vida;
	private ProjectilEnemic projectil;
	
	public Enemic(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f,int vida, ProjectilEnemic pe) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.vida = vida;
		this.projectil = pe;
	}
	
	@Override
	public void onCollisionEnter(Sprite sprite) {
		// TODO Auto-generated method stub	
	}
	@Override
	public void onCollisionExit(Sprite sprite) {
		// TODO Auto-generated method stub
	}

	@Override
	public void danyar() {
		this.vida--;
		if (this.vida <= 0) {
			this.delete();
		}
	}
	
	public void disparar() {
		if (f.containsSprite(this)) {
			ProjectilEnemic projectil = new ProjectilEnemic(this.projectil, this.x1-50, this.y1+50, this.x1, this.y1+100);
		}
		
	}
}
