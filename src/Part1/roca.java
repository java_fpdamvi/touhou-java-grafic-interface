package Part1;
import java.awt.Color;

import Core.Field;
import Core.Sprite;
import Part2.Joc2;

public class roca extends Sprite{
	public static int comptador = 0;
	private int id;
	private int accionsDisponibles;
	
	public int getId() {
		return id;
	}
	public int getAccionsDisponibles() {
		return accionsDisponibles;
	}

	public void setAccionsDisponibles(int accionsDisponibles) {
		this.accionsDisponibles = accionsDisponibles;
	}

	//CONSTRUCTORS:
	public roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.comptador++;
		this.id = comptador;
		this.name = this.name+id;
	}
	public roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int accions) {
	    super(name, x1, y1, x2, y2, angle, path, f);  
	    this.accionsDisponibles = accions;
	    this.comptador++;
	    this.id = comptador;
	}
	public roca(int x1, int y1, int x2, int y2,Field f,int accions) {
		super("Roca", x1, y1, x2, y2,0,"resources/rock1.png",f);
		this.accionsDisponibles = accions;
		this.comptador++;
		this.id = comptador;
	}
	public roca(int x1, int y1, int mida,Field f) {
		super("Roca", x1, y1, x1+mida, y1+mida,0,"resources/rock1.png",f);
		this.accionsDisponibles = 50;
		this.comptador++;
		this.id = comptador;
	}
	public roca() {
		super("Roca",0,0,50,50,0,"resources/rock1.png",Joc2.f);
		this.accionsDisponibles = 50;
		comptador++;
		this.id = comptador;
	}
}
