package Part1;
import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class Joc1 {
	static Field f = new Field();
	static Window w = new Window(f);
	public static void main(String[] args) throws InterruptedException{
		roca roca1 = new roca("roca",50,50,150,150,0,"resources/rock1.png",f);
		roca roca2 = new roca("roca",150,150,250,250,0,"resources/rock1.png",f,50);
		roca roca3 = new roca(250,250,350,350,f,50);
		roca roca4 = new roca(350,350,200,f);
		roca roca5 = new roca();
		ArrayList<Sprite> list = new ArrayList<Sprite>();
		list.add(roca1);
		list.add(roca2);
		list.add(roca3);
		list.add(roca4);
		list.add(roca5);
		System.out.println(roca4.getId());
		boolean exit = false;
		while(!exit) {
			f.draw();
			Thread.sleep(30);
		}
	}
}
